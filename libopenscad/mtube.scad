// BOF

// NOSTL

module mtube(height, od, id, align = [0, 0, 0], chamfer = 0) {

    translate([(od / 2) * align.x, (od / 2) * align.y, (height / 2) * (align.z - 1)]) {

        rotate_extrude() {
            polygon(points = [
            [id / 2          , chamfer],
            [id / 2 + chamfer, 0      ],
            [od / 2 - chamfer, 0      ],
            [od / 2          , chamfer],
            [od / 2          , height - chamfer],
            [od / 2 - chamfer, height],
            [id / 2 + chamfer, height],
            [id / 2          , height - chamfer]
            ]
            );
        }
    }
}







if(!is_undef(debug_lib)) {

    mtube(height = 40, od = 40, id = 30, chamfer = (40 - 30) / 2 / 4);
    mtube(height = 20, od = 60, id = 50, chamfer = 1);

}


echo("\n
\nmtube usage:
\n\tmodule mtube(height, od, id, align = [0, 0, 0], chamfer = 0)
\n
\n
");



// EOF
