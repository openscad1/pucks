include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;
include <puck.scad>;


show_test_parts = false;
show_test_pucks = false;


module box_puck() {
    difference() {
        union() {
            crossbar();
            bottom_half_puck();
        }
        pins();
    }
}


box_puck();


// EOF   
