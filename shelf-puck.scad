include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;
include <puck.scad>;


show_test_parts = false;
show_test_pucks = false;


module shelf_puck() {
    
    difference() {
        union() {
            difference() {
                puck();
                crossbar();
            }
            translate([0, 0, - height / 4]) {
                half_puck();
            }
        }
        union() {
            rotate([0, 0, 45]) {
                pins(diameter = diameter, height = height);
            }
        }
    }
}

shelf_puck();

//color("cyan") crossbar();
// EOF   
