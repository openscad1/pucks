include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;
include <puck.scad>;


show_test_parts = false;
show_test_pucks = false;


module shelf_puck() {
    
    difference() {
        union() {
            difference() {
                puck();
                crossbar();
            }
            translate([0, 0, - height / 4]) {
                half_puck();
            }
        }
        union() {
            rotate([0, 0, 45]) {
                pins(diameter = diameter, height = height);
            }
        }
    }
}

//through_diameter = 4;
//shoulder_diameter = 7;

//shelf_puck();

//color("cyan") crossbar();
// EOF   

$fn = 100;

rotate([180, 0, 0]) {
    translate([25, 0, -2]) {
        color("blue") {
            mcylinder(h = 8, d = shoulder_diameter - 0.5, chamfer = 0.5);
        }
    }


    translate([25, 0, 0]) {
        color("green") {
            mcylinder(h = 7, d = shoulder_diameter + 12, chamfer = 0.5 );
        }
    }
}