// NOSTL

include <libopenscad/mcube.scad>;
include <libopenscad/mcylinder.scad>;


// true
// false

show_test_pucks = true;
show_test_parts = false;

diameter = 3.1 * 25.4;
height = 1 * 25.4;
chamfer_ratio = 1/8;

//$fn = 180;

cutout_ratio = 1/4;

through_diameter = 4;
shoulder_diameter = 7;


//     echo(diameter * cutout_ratio);


module puck() {
    
    module top_half_puck() {
        color("salmon") {
            intersection() {
                puck();
                translate([0, 0, -0.01]) { // overlap
                    mcylinder(h = height, d = diameter + 1);
                }
            }
        }
    }
    
    module bottom_half_puck(height = height, diameter = diameter) {
        color("fuchsia") {
            intersection() {
                puck();
                translate([0, 0, -height]) {
                    translate([0, 0, 0.01]) { // overlap
                        mcylinder(h = height, d = diameter);
                    }
                }
            }
        }
    }

    color("orange") {
        mcylinder(h = height, d = diameter, chamfer = height * chamfer_ratio, center = true);
    }
    
}


module half_puck() {
    color("orange") {
        mcylinder(h = height/2, d = diameter, chamfer = height * chamfer_ratio, center = true);
    }
}







module lobes(crossbar_width = 0, height = height) {

    cbw = crossbar_width ? crossbar_width : diameter * cutout_ratio;
            for(i = [0, 90, 180, 270]) {
                rotate([0, 0, i]) {
                    color("red") {
                        translate([cbw , cbw, 0]) {
                            hull() {
                                translate([             0,              0, 0]) cylinder(h = height + 2, d = cbw, center = true);
                                translate([diameter/4 + 1,              0, 0]) cylinder(h = height + 2, d = cbw, center = true);
                                translate([             0, diameter/4 + 1, 0]) cylinder(h = height + 2, d = cbw, center = true);
                                translate([diameter/4 + 1, diameter/4 + 1, 0]) cylinder(h = height + 2, d = cbw, center = true);
                            }
                        }
                    }
                }
            }


}





module crossbar() {
        module crossbar_cutout_block() {
        color("red") {
            for(z = [-1, 1]) {
                hull() {
                    translate([0, 0, z * (height -(height * chamfer_ratio))]) {
                        for(x = [-1, 1]){ 
                            for(y = [-1, 1]) {
                                translate([x * height, y * height, 0]) {
                                    color("green") {
                                        mcylinder(d = diameter * cutout_ratio, h = height, chamfer = height * chamfer_ratio, center = true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            hull() {
                for(x = [-1, 1]) { 
                    for(y = [-1, 1]) {
                        translate([x * height, y * height, 0]) {
                            mcylinder(d = diameter * cutout_ratio - (height * chamfer_ratio * 2), h = height * 2, center = true);
                        }
                    }
                }
            }
        }
    }

    intersection() {
        difference() {
            union() color("green") {
                mcube([diameter + height, diameter + height, height], center = true, chamfer = height * chamfer_ratio);
            }

            union() {
                for(x = [-1, 1]){ 
                    for(y = [-1, 1]) {
                        translate([x * (height + (diameter * cutout_ratio) - (height * chamfer_ratio)), y * (height + (diameter * cutout_ratio) - (height * chamfer_ratio)), 0]) {
                            crossbar_cutout_block();
                        }
                    }
                }
            }
        }

        puck();
    }
}



module pins(height = height, diameter = diameter) {
    
    module pin(height = height, diameter = diameter) {
        mcylinder(h = height * 2, d = through_diameter, center = true);
        translate([0, 0, height - height / 4]) {
            mcylinder(h = height * 2, d = shoulder_diameter, center = true);
        }
    }


    color("cyan") {
        pin(height = height, diameter = diameter);




        for(i = [0, 90, 180, 270]) {
            rotate([0, 0, i]) {
                translate([diameter * cutout_ratio , diameter * cutout_ratio, 0]) {
                    pin(height = height, diameter = diameter);
                }
            }
        }
    }
}


module tread_slug() {

        cube([19, 100, 254], center = true);
}


if(show_test_pucks) {

    translate([0, 50, 0]) {
        tread_slug();
    }

    // pucks
    //
    translate([ 50,  0, 0]) {
        puck();
        
        translate([0, -50, 0]) {
            color("pink") {
                puck();
            }
        }
    }

    // lobes
    //
    translate([  0, 50, 0]) {
        lobes();
        translate([  0, 50, 0]) {
            translate([  0, 75, 0]) {
                crossbar();
            }
        }
    }

    // pins
    //

    translate([-100,  0, 0]) {
        union() {
            pins();
            lobes();
            half_puck();
        }
    }
    shelf_puck();

}



    module shelf_puck() {
        
        difference() {
            union() {
                difference() {
                    puck();
                    crossbar();
                }
                translate([0, 0, - height / 4]) {
                    half_puck();
                }
            }
            union() {
                rotate([0, 0, 45]) {
                    pins(diameter = diameter, height = height);
                }
            }
        }
    }




// EOF
